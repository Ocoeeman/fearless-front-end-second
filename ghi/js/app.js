function createCard(title, description, pictureUrl, sDate, eDate) {
    return `
        <div class="col align-self-start">
          <div class="card shadow mt-2">
            <img src="${pictureUrl}" class="card-img-top">
              <div class="card-body">
              <h5 class="card-title">${title}</h5>
              <p class="card-text">${description}</p>
              </div>
              <div class="card-footer text-muted">
              <h6 class="card-subtitle mb-2 text-muted ">${sDate}-${eDate}</h6>
              </div>
          </div>
        </div>
    `;
  }


function createAlert() {
    return `
    <div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
    </div>
    <div class="alert alert-secondary" role="alert">
        A simple secondary alert—check it out!
    </div>
    <div class="alert alert-success" role="alert">
        A simple success alert—check it out!
    </div>
    <div class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
    </div>
    <div class="alert alert-warning" role="alert">
        A simple warning alert—check it out!
    </div>
    <div class="alert alert-info" role="alert">
        A simple info alert—check it out!
    </div>
    <div class="alert alert-light" role="alert">
        A simple light alert—check it out!
    </div>
    <div class="alert alert-dark" role="alert">
        A simple dark alert—check it out!
    </div>
    `;
}


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const srtDate = new Date(details.conference.starts);
            const sDate = `${srtDate.getMonth()}/${srtDate.getDate()}/${srtDate.getFullYear()}`;
            const endDate = new Date(details.conference.ends);
            const eDate = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`;
            const html = createCard(title, description, pictureUrl, sDate, eDate);
            const row = document.querySelector('.row');
            row.innerHTML += html;
            }
        }

      }
    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
        const alert = document.querySelector('.alert')
        alert.innerHTML = createAlert()
      }

  });
